# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.

def guessing_game

  number = 1 + rand(100)
  guess = 0
  guesses = 0

  until guess == number

    puts "guess a number"
    guess = gets.chomp.to_i
    guesses += 1

    if guess < number
      puts "#{guess} is too low"
    elsif guess > number
      puts "#{guess} is too high"
    else
      puts "#{guess} is correct, #{guesses} guesses"
    end
  end
end


def shuffle_file
  print "Enter filename: "
  filename = gets.chomp

  lines = File.readlines(filename).shuffle

  File.open("#{filename}-shuffled.txt", "w") do |f|
    lines.each do |line|
      f.print(line)
    end
  end
end

if __FILE__ == $PROGRAM_NAME
  shuffle_file
end
